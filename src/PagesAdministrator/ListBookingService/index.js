import "./index.css";
import {
  Input,
  Row,
  Col,
  Button,
  Table,
  Tag,
  Spin,
  DatePicker,
  Drawer,
} from "antd";
import { compose } from "recompose";
import { connect } from "react-redux";
import { asyncGetAllListBookingServicesAction } from "./stores/action";
import { createStructuredSelector } from "reselect";
import { useEffect, useState } from "react";
import { selectListBookingServices } from "./stores/selector";
import { Space } from "antd";

//object destructuring. '={}' to allow function to be call without params requestPayload()
function requestPayload({ pageIndex = 1, pageSize = 10, search = "" } = {}) {
  const payload = {
    search: search,
    startAt: null,
    endAt: null,
    paging: {
      pageIndex: pageIndex,
      pageSize: pageSize,
    },
    sorting: {
      field: "chooseDate",
      order: "desc",
    },
  };
  return payload;
}

function ListBookingServicesPage(props) {
  const { getAllListBookingServicesDispatch } = props;
  const { listListBookingServices } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [bookingData, setBookingData] = useState({});
  useEffect(() => {
    const loadData = async () => {
      await getAllListBookingServicesDispatch(requestPayload());
      setIsLoading(false);
    };
    loadData();
  }, []);

  const tableData = () => {
    const pageIndex = listListBookingServices.paging?.pageIndex;
    const pageSize = listListBookingServices.paging?.pageSize;
    const data = listListBookingServices.data?.map((item, index) => {
      return {
        ...item,
        key: item.id,
        displayId: (pageIndex - 1) * pageSize + 1 + index,
      };
    });
    return data;
  };
  const tablePagination = () => {
    const pageIndex = listListBookingServices.paging?.pageIndex;
    const pageSize = listListBookingServices.paging?.pageSize;
    const total = listListBookingServices.paging?.total;
    const defaultPageSize = 10;
    const current = listListBookingServices.paging?.pageIndex;

    return { defaultPageSize, pageIndex, pageSize, total, current };
  };
  const onChangeTable = async (paging) => {
    if (paging) {
      setIsLoading(true);
      await getAllListBookingServicesDispatch(
        requestPayload({ pageIndex: paging?.current })
      );
      setIsLoading(false);
    }
  };
  //Search box
  const { Search } = Input;
  const onSearch = async (value) => {
    setIsLoading(true);
    await getAllListBookingServicesDispatch(requestPayload({ search: value }));
    setIsLoading(false);
  };

  const showBookingDetail = (data) => {
    setOpenDrawer(true);
    setBookingData(data);
  };
  //Table section
  const columns = [
    {
      title: "ID",
      dataIndex: "displayId",
      key: "displayId",
    },
    {
      title: "Service Name",
      dataIndex: "serviceName",
      key: "name",
      render: (text, record) => (
        <Button type="link" onClick={() => showBookingDetail(record)}>
          {text}
        </Button>
      ),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (text) => {
        return text === "APPROVED" ? (
          <Tag color="green"> {text} </Tag>
        ) : (
          <Tag color="volcano"> {text} </Tag>
        );
      },
    },
    {
      title: "Created By",
      dataIndex: "createdBy",
      key: "createdBy",
    },
    {
      title: "Booking Date",
      dataIndex: "chooseDate",
      key: "chooseDate",
    },
    {
      title: "Booking Time",
      dataIndex: "bookingTime",
      key: "bookingTime",
    },
  ];

  return (
    <Spin className="news-container" spinning={isLoading}>
      <p className="news-breadcrumb">
        Home / Services /{" "}
        <span className="current-breadcrumb">Booking Services</span>
      </p>
      <h1 className="news-title">Booking Services</h1>
      <Row>
        <Col span={8}>
          <Search
            placeholder="Search by service's name"
            onSearch={onSearch}
            enterButton
          />
        </Col>
        <Col span={8} offset={8} style={{ textAlign: "right" }}>
          <Space style={{ fontSize: "17px" }}>
            Filter: <DatePicker.RangePicker picker="week" />
          </Space>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={tableData()}
        pagination={tablePagination()}
        onChange={onChangeTable}
        className="news-table"
      />
      <Drawer
        title="Booking Detail"
        placement="right"
        onClose={() => setOpenDrawer(false)}
        closable={false}
        visible={openDrawer}
        key="booking-detail"
        width={450}
        style={{ fontSize: "16px" }}
      >
        <p>
          <strong>Service Name: </strong> {bookingData.serviceName}
        </p>
        <p>
          <strong>Booking Date: </strong> {bookingData.chooseDate}
        </p>
        <p>
          <strong>Booking Time: </strong> {bookingData.bookingTime}
        </p>
        <p>
          <strong>Status: </strong> {bookingData.status}
        </p>
        <p>
          <strong>Create By: </strong> {bookingData.createdBy}
        </p>
        <p>
          <strong>Create At: </strong> {bookingData.createdAt}
        </p>
      </Drawer>
    </Spin>
  );
}

const mapStateToProps = createStructuredSelector({
  listListBookingServices: selectListBookingServices,
});

const mapDispatchToProps = (dispatch) => ({
  getAllListBookingServicesDispatch: (payload) =>
    asyncGetAllListBookingServicesAction(dispatch)(payload),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(ListBookingServicesPage);
