import "./index.css";
import { Input, Row, Col, Button, Table, Popconfirm, Spin } from "antd";
import { PaperClipOutlined } from "@ant-design/icons";
import { compose } from "recompose";
import { connect } from "react-redux";
import { asyncDeleteNewsAction, asyncGetAllNewsAction } from "./stores/action";
import { createStructuredSelector } from "reselect";
import { useEffect, useState } from "react";
import { selectNews } from "./stores/selector";
import { Link } from "react-router-dom";

//object destructuring. '={}' to allow function to be call without params requestPayload()
function requestPayload({ pageIndex = 1, pageSize = 10, search = "" } = {}) {
  const payload = {
    search: search,
    paging: {
      pageIndex: pageIndex,
      pageSize: pageSize,
    },
    sorting: {
      field: "createdAt",
      order: "desc",
    },
  };
  return payload;
}

function NewsPage(props) {
  const { getAllNewsDispatch, deleteNewsDispatch } = props;
  const { listNews } = props;
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    const loadData = async () => {
      await getAllNewsDispatch(requestPayload());
      setIsLoading(false);
    };
    loadData();
  }, []);

  const deleteNews = async (id) => {
    const resolve = await deleteNewsDispatch(id);
    if (resolve.status === 200) {
      setIsLoading(true);
      await getAllNewsDispatch(requestPayload());
      setIsLoading(false);
    }
  };

  const tableData = () => {
    const pageIndex = listNews.paging?.pageIndex;
    const pageSize = listNews.paging?.pageSize;
    const data = listNews.data?.map((item, index) => {
      return {
        ...item,
        key: item.id,
        displayId: (pageIndex - 1) * pageSize + 1 + index,
      };
    });
    return data;
  };
  const tablePagination = () => {
    const pageIndex = listNews.paging?.pageIndex;
    const pageSize = listNews.paging?.pageSize;
    const total = listNews.paging?.total;
    const defaultPageSize = 10;
    const current = listNews.paging?.pageIndex;

    return { defaultPageSize, pageIndex, pageSize, total, current };
  };
  const onChangeTable = async (paging) => {
    if (paging) {
      setIsLoading(true);
      await getAllNewsDispatch(requestPayload({ pageIndex: paging?.current }));
      setIsLoading(false);
    }
  };
  //Search box
  const { Search } = Input;
  const onSearch = async (value) => {
    setIsLoading(true);
    await getAllNewsDispatch(requestPayload({ search: value }));
    setIsLoading(false);
  };

  //Table section
  const columns = [
    {
      title: "ID",
      dataIndex: "displayId",
      key: "displayId",
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
      render: (text, record) => (
        <Link
          to={`/administrator/news/edit/${record.id}`}
          className="news-link"
        >
          {text}
        </Link>
      ),
    },
    {
      title: "Tag",
      dataIndex: "tag",
      key: "tag",
    },
    {
      title: "Create at",
      dataIndex: "createdAt",
      key: "createdAt",
    },
    {
      title: "Create by",
      dataIndex: "createdBy",
      key: "createdBy",
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Popconfirm
          title="Are you sure?"
          okText="Yes"
          cancelText="No"
          onConfirm={() => deleteNews(record.id)}
        >
          <Button type="link">Delete</Button>
        </Popconfirm>
      ),
    },
  ];

  return (
    <Spin className="news-container" spinning={isLoading}>
      <p className="news-breadcrumb">
        Home / <span className="current-breadcrumb">News</span>
      </p>
      <h1 className="news-title">News Management</h1>
      <Row>
        <Col span={8}>
          <Search
            placeholder="Search by title or tag"
            onSearch={onSearch}
            enterButton
          />
        </Col>
        <Col span={4} offset={12} style={{ textAlign: "right" }}>
          <Link to="/administrator/news/create">
            <Button type="primary">
              <PaperClipOutlined />
              Create News
            </Button>
          </Link>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={tableData()}
        pagination={tablePagination()}
        onChange={onChangeTable}
        className="news-table"
      />
    </Spin>
  );
}

const mapStateToProps = createStructuredSelector({
  listNews: selectNews,
});

const mapDispatchToProps = (dispatch) => ({
  getAllNewsDispatch: (payload) => asyncGetAllNewsAction(dispatch)(payload),
  deleteNewsDispatch: (payload) => asyncDeleteNewsAction(dispatch)(payload),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(NewsPage);
