import "./NewsForm.css";
import { UploadOutlined } from "@ant-design/icons";
import { Button, Upload, Form, Input, Row, Col, Image, Spin } from "antd";
import React, { useState, useEffect } from "react";

import { compose } from "recompose";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import {
  asyncCreateNewsAction,
  asyncGetDetailNewsAction,
  asyncUpdateNewsAction,
} from "../stores/action";
import { useNavigate, useParams } from "react-router-dom";

function NewsForm(props) {
  const { createNewsDispatch, getDetailNewsDispatch, updateNewsDispatch } =
    props;
  const [fileList, setFileList] = useState([]);
  const [formImage, setFormImage] = useState("");
  const [form] = Form.useForm();
  const { id } = useParams();
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  useEffect(() => {
    const getFormDetail = async () => {
      setIsLoading(true);
      const response = await getDetailNewsDispatch(id);
      if (response.status === 200) {
        form.setFieldsValue({
          title: response.data.result.title,
          tag: response.data.result.tag,
          description: response.data.result.description,
          id: response.data.result.id,
        });
        setFormImage(
          `data:image/png;charset=utf-8;base64, ${response.data.result.imageUrl}`
        );
      }
      setIsLoading(false);
    };
    if (!props.onCreate) {
      getFormDetail();
    }
  }, []);
  const uploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      setFileList(newFileList);
    },
    beforeUpload: (file) => {
      setFileList([...fileList, file]);
      return false;
    },
    fileList,
  };

  const onFinish = async (values) => {
    let news = form.getFieldValue();
    let request = new FormData();
    request.append("file", fileList[0]);
    request.append("title", news.title || "");
    request.append("description", news.description || "");
    request.append("tag", news.tag || "");
    if (props.onCreate) {
      const resolve = await createNewsDispatch(request);
      if (resolve === 200) {
        navigate("/administrator/news");
      }
    } else {
      request.append("id", news.id);
      const resolve = await updateNewsDispatch(request);
      if (resolve.status === 200) {
        navigate("/administrator/news");
      }
    }
  };

  return (
    <Spin spinning={isLoading}>
      <p className="news-breadcrumb">
        Home / <span className="current-breadcrumb">News</span>
      </p>
      <h1 className="news-title">News Management</h1>
      <Form
        form={form}
        name="form"
        labelCol={{
          span: 2,
        }}
        wrapperCol={{
          span: 22,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
        layout="horizontal"
      >
        <Form.Item label="Id" name="id" style={{ display: "none" }}>
          <Input />
        </Form.Item>
        <Form.Item label="Image" name="image">
          <Upload {...uploadProps}>
            <Button icon={<UploadOutlined />}>Select File</Button>
          </Upload>
          <Image
            width={200}
            src={formImage}
            style={{ display: props.onCreate ? "none" : "", paddingTop: 20 }}
          />
        </Form.Item>
        <Form.Item
          label="Title"
          name="title"
          rules={[
            {
              required: true,
              message: "Please input content title!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Tag"
          name="tag"
          rules={[
            {
              required: true,
              message: "Please input tag!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Content"
          name="description"
          rules={[
            {
              required: true,
              message: "Please input content!",
            },
          ]}
        >
          <Input.TextArea rows={10} />
        </Form.Item>

        <Form.Item align="baseline">
          <Row gutter={24}>
            <Col offset={24}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Col>
          </Row>
        </Form.Item>
      </Form>
    </Spin>
  );
}

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = (dispatch) => ({
  createNewsDispatch: (payload) => asyncCreateNewsAction(dispatch)(payload),
  getDetailNewsDispatch: (payload) =>
    asyncGetDetailNewsAction(dispatch)(payload),
  updateNewsDispatch: (payload) => asyncUpdateNewsAction(dispatch)(payload),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(NewsForm);
