import "./index.css";
import { Input, Row, Col, Button, Table, Tag, Popconfirm, Spin } from "antd";
import { PaperClipOutlined } from "@ant-design/icons";
import { compose } from "recompose";
import { connect } from "react-redux";
import {
  asyncDeleteSystemServicesAction,
  getAllSystemServicesAction,
  asyncGetAllSystemServicesAction,
} from "./stores/action";
import { createStructuredSelector } from "reselect";
import { useEffect, useState } from "react";
import { selectSystemServices } from "./stores/selector";
import { Link } from "react-router-dom";
import { withTranslation } from "react-i18next";

//object destructuring. '={}' to allow function to be call without params requestPayload()
function requestPayload({ pageIndex = 1, pageSize = 10, search = "" } = {}) {
  const payload = {
    search: search,
    paging: {
      pageIndex: pageIndex,
      pageSize: pageSize,
    },
    sorting: {
      order: "desc",
    },
  };
  return payload;
}

function SystemServicesPageComponent(props) {
  const { getAllSystemServicesDispatch, deleteSystemServicesDispatch } = props;
  const { listSystemServices } = props;
  const [isLoading, setIsLoading] = useState(true);
  const { t } = props;
  useEffect(() => {
    const loadData = async () => {
      await getAllSystemServicesDispatch(requestPayload());
      setIsLoading(false);
    };
    loadData();
  }, []);

  const deleteSystemServices = async (id) => {
    const resolve = await deleteSystemServicesDispatch(id);
    if (resolve.status === 200) {
      setIsLoading(true);
      await getAllSystemServicesDispatch(requestPayload());
      setIsLoading(false);
    }
  };

  const tableData = () => {
    const pageIndex = listSystemServices.paging?.pageIndex;
    const pageSize = listSystemServices.paging?.pageSize;
    const data = listSystemServices.data?.map((item, index) => {
      return {
        ...item,
        key: item.id,
        displayId: (pageIndex - 1) * pageSize + 1 + index,
      };
    });

    return data;
  };
  const tablePagination = () => {
    const pageIndex = listSystemServices.paging?.pageIndex;
    const pageSize = listSystemServices.paging?.pageSize;
    const total = listSystemServices.paging?.total;
    const defaultPageSize = 10;
    const current = listSystemServices.paging?.pageIndex;

    return { defaultPageSize, pageIndex, pageSize, total, current };
  };
  const onChangeTable = async (paging) => {
    if (paging) {
      setIsLoading(true);
      await getAllSystemServicesDispatch(
        requestPayload({ pageIndex: paging?.current })
      );
      setIsLoading(false);
    }
  };
  //Search box
  const { Search } = Input;
  const onSearch = async (value) => {
    setIsLoading(true);
    await getAllSystemServicesDispatch(requestPayload({ search: value }));
    setIsLoading(false);
  };

  //Table section
  const columns = [
    {
      title: "ID",
      dataIndex: "displayId",
      key: "displayId",
    },
    {
      title: t("ServiceName"),
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link
          to={`/administrator/services/system/edit/${record.id}`}
          className="news-link"
        >
          {text}
        </Link>
      ),
    },
    {
      title: t("ServicesType"),
      dataIndex: "type",
      key: "type",
      render: (text) => <Tag color="orange"> {text} </Tag>,
    },
    {
      title: t("Status"),
      dataIndex: "status",
      key: "status",
      render: (text) => <Tag color="blue"> {text} </Tag>,
    },
    {
      title: t("Action"),
      key: "action",
      render: (_, record) => (
        <Popconfirm
          title="Are you sure?"
          okText="Yes"
          cancelText="No"
          onConfirm={() => deleteSystemServices(record.id)}
        >
          <Button type="link">{t("Delete")}</Button>
        </Popconfirm>
      ),
    },
  ];

  return (
    <Spin className="news-container" spinning={isLoading}>
      <p className="news-breadcrumb">
        {t("Home")} / {t("ServicesTitle")} / <span className="current-breadcrumb">{t("ListServices")}</span>
      </p>
      <h1 className="news-title">{t("ListServicesTitle")}</h1>
      <Row>
        <Col span={8}>
          <Search
            placeholder={t("BookingPlaceholder")}
            onSearch={onSearch}
            enterButton
          />
        </Col>
        <Col span={4} offset={12} style={{ textAlign: "right" }}>
          <Link to="/administrator/services/system/create">
            <Button type="primary">
              <PaperClipOutlined />
              {t("CreateServices")}
            </Button>
          </Link>
        </Col>
      </Row>
      <Table
        columns={columns}
        dataSource={tableData()}
        pagination={tablePagination()}
        onChange={onChangeTable}
        className="news-table"
      />
    </Spin>
  );
}

const mapStateToProps = createStructuredSelector({
  listSystemServices: selectSystemServices,
});

const mapDispatchToProps = (dispatch) => ({
  getAllSystemServicesDispatch: (payload) =>
    asyncGetAllSystemServicesAction(dispatch)(payload),
  deleteSystemServicesDispatch: (payload) =>
    asyncDeleteSystemServicesAction(dispatch)(payload),
});

//const withConnect = connect(mapStateToProps, mapDispatchToProps);
const SystemServicesPage = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(SystemServicesPageComponent)
)
export default SystemServicesPage;
